/*
	SERVO demonstration program
	middle.c

	(C)1999 Eric Shalov.
*/

#include <stdio.h>
#include <unistd.h>
#include <math.h>

#include "servo.h"

char servo_portname[] = "/dev/ttyS0";

unsigned int max_range = 90;

int main(int argc, char *argv[]) {
	SERVO		*servo_port;

	if( (servo_port = servo_init(servo_portname,max_range)) ){
		servo_set(servo_port, 0, 128);
		servo_set(servo_port, 1, 128);
		servo_set(servo_port, 2, 128);
		servo_shutdown(servo_port);
	} else {
		fprintf(stderr,"Unable to open servo interface.\n");
		exit(1);
	}
}
