libservossc2
============
C Library for Linux to control Mini SSC II server controller board. 
February, 1999.

![ssc2.png](ssc2.png "ssc2.png")

Files
-----
servo.c		Servo library
servo.h		Servo include
shake.c		Sample program ("shake")

License:
--------
libservossc2 is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
