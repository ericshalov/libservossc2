/*
	SERVO demonstration program
	main.c

	(C)1999 Eric Shalov.
	This work is copyrighted under the GNU public license.
*/

#include <stdio.h>
#include <unistd.h>
#include <math.h>

#include "servo.h"

char servo_portname[] = "/dev/ttyS0";

unsigned int max_range = 90;

int main(int argc, char *argv[]) {
	float		position = 45.0,
			velocity = 0.50;
	unsigned char	servo = 0,
			iposition;
	SERVO		*servo_port;

	if( (servo_port = servo_init(servo_portname,max_range)) ){
		while(1) {
			if(fabs(velocity) < 10.0) velocity *= 1.005;
			position += velocity;
			if(position<0) {
				velocity *= -1.0;
				position=0.0;
			} else if(position>max_range) {
				velocity *= -1.0;
				position=max_range;
			}

			usleep(10000); /* 1/100th's of a second */

			printf("velocity: %6.2f    position: %6.2f degrees>  ",
				velocity, position);
			{
				int i;
				for(i=0;i<20;i++)
				putchar((position/max_range)>((float)i/20)?'+':'-');
			}
			putchar('\r');
			fflush(stdout);

			iposition = (unsigned char)((float)255.0*position/max_range);
			servo_set(servo_port, 0, iposition);
			servo_set(servo_port, 1, iposition);
			servo_set(servo_port, 2, iposition);
			servo_set(servo_port, 3, iposition);
			servo_set(servo_port, 4, iposition);
			servo_set(servo_port, 5, iposition);
		}
		
		servo_shutdown(servo_port);
	} else {
		fprintf(stderr,"Unable to open servo interface.\n");
		exit(1);
	}
}
