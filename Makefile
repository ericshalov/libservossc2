#
#	Makefile for libservossc2 library
#
#	(C) 1999 Eric Shalov.

CC	= gcc
CCFLAGS	= -Wall

#############################################################################

BINS	= shake test middle servoset
OBJS	= shake.o servo.o servoset.o test.o middle.o

default:	all

all:		$(BINS)

clean:
		rm -f $(BINS) $(OBJS)

shake:		shake.o servo.o

servo.o:	servo.c

shake.o:	shake.c

test:		test.o servo.o

test.o:		test.c

middle:		middle.o servo.o

middle.o:	middle.c

servoset:	servoset.o servo.o

servoset.o:	servoset.c
