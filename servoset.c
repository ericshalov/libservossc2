/*
	servoset.c

	(C)1999 Eric Shalov.
*/

#include <stdio.h>
#include <unistd.h>
#include <math.h>

#include "servo.h"

char servo_portname[] = "/dev/ttyS0";

unsigned int max_range = 90;

int main(int argc, char *argv[]) {
	float		position = 45.0,
			velocity = 0.50;
	unsigned char	servo = 0,
			iposition;
	SERVO		*servo_port;

	servo = atoi(argv[1]);
	iposition = atoi(argv[2]);

	if( (servo_port = servo_init(servo_portname,max_range)) ){
		servo_set(servo_port, servo, iposition);
		servo_shutdown(servo_port);
	} else {
		fprintf(stderr,"Unable to open servo interface.\n");
		exit(1);
	}
}
