/*

	servo.c
	libservossc2 - Control library for Mini SSC II servo controller.

	(C)1999 Eric Shalov.
*/

#include <unistd.h>
#include <malloc.h>
#include <termios.h>
#include <fcntl.h>
#include "servo.h"

SERVO *servo_init(char *servo_portname, unsigned int max_range) {
	SERVO s, *new;
	struct termios t;

	s.fd = open(servo_portname,O_WRONLY);
	s.position = 0;
	s.max_range = max_range;
	tcgetattr(s.fd,&t);
	if(t.c_iflag & IXON) t.c_iflag ^= IXON;        /* turn off xon/xoff */
	if(t.c_cflag & CRTSCTS) t.c_cflag ^= CRTSCTS;  /* turn off rts/cts */
	cfsetospeed(&t,B9600);      /* set output speed to 9600 */
	tcsetattr(s.fd,TCSANOW,&t); /* changes take effect immediately */

	new = malloc(sizeof(SERVO));
	memcpy(new,&s,sizeof(SERVO));
	
	return new;
}

void servo_shutdown(SERVO *s) {
	close(s->fd);
}

void servo_set(SERVO *s, int servo, int iposition) {
		static unsigned char buffer[3];

		buffer[0] = 255;
		buffer[1] = servo;
		buffer[2] = iposition;

		s->position = iposition;

		write(s->fd,buffer,3);
}
