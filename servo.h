/*
	SERVO Library Include
	servo.h

	(C)1999 Eric Shalov.
*/

typedef struct {
	int fd;
	unsigned int position;   /* last known position */
	unsigned int max_range;  /* 90 or 180 degrees */
} SERVO;

SERVO *servo_init(char *servo_portname, unsigned int max_range);
void servo_shutdown(SERVO *s);
void servo_set(SERVO *s, int servo, int iposition);
